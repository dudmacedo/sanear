package sanear;

import java.awt.EventQueue;

import sanear.gui.Principal;
import sanear.persistencia.Persistencia;

public class App {
	public static void main(String[] args) {
		Persistencia persistencia = new Persistencia();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal(persistencia);
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
