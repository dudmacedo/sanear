package sanear.entidade;

import java.io.Serializable;

public class Cidade implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5936933245464531316L;
	private String nome;
	private Estado uf;
	
	public Cidade(String nome, Estado uf) {
		this.nome = nome;
		this.uf = uf;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public void setUF(Estado uf) {
		this.uf = uf;
	}
	public Estado getUF() {
		return uf;
	}
	
	@Override
	public String toString() {
		return nome;
	}
}
