package sanear.entidade;

import java.io.Serializable;

public abstract class Cliente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1071892681933331568L;
	protected String nu_ident;
	protected String nome;
	protected String endereco;
	protected String complemento;
	protected Cidade cidade;
	
	public Cliente(String nu_ident, String nome, String endereco, String complemento, Cidade cidade) throws SanearEntidadeException {
		setNumeroIdentificador(nu_ident);
		this.nome = nome;
		this.endereco = endereco;
		this.complemento = complemento;
		this.cidade = cidade;
	}
	
	protected abstract void setNumeroIdentificador(String nu_ident) throws SanearEntidadeException;
	
	public boolean isIgual(Cliente cliente) {
		if (
				nu_ident.equals(cliente.getNumeroIdentificador()) &&
				nome.equals(cliente.getNome()) &&
				endereco.equals(cliente.getEndereco()) &&
				complemento.equals(cliente.getComplemento()) &&
				cidade.getNome().equals(cliente.getCidade().getNome()) &&
				cidade.getUF() == cliente.getCidade().getUF()
				) {
			return true;
		}
		
		return false;
	}
	
	public String getNumeroIdentificador() {
		return nu_ident;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public Cidade getCidade() {
		return this.cidade;
	}
}
