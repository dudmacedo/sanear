package sanear.entidade;

import java.io.Serializable;

public class Conta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2525460948707959192L;
	private int numero;
	private String endereco;
	private String complemento;
	private Hidrometro hidrometro;
	private Cidade cidade;
	private SituacaoConta situacao;
	private Cliente proprietario;
	private Cliente titular;
	
	public Conta(int numero, String endereco, String complemento, Hidrometro hidrometro,
			Cidade cidade, SituacaoConta situacao, Cliente proprietario, Cliente titular) {
		this.setNumero(numero);
		this.setEndereco(endereco);
		this.complemento = complemento;
		this.hidrometro = hidrometro;
		this.cidade = cidade;
		this.situacao = situacao;
		this.proprietario = proprietario;
		this.titular = titular;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getNumero() {
		return numero;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setHidrometro(Hidrometro hidrometro) {
		this.hidrometro = hidrometro;
	}
	public Hidrometro getHidrometro() {
		return hidrometro;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setSituacao(SituacaoConta situacao) {
		this.situacao = situacao;
	}
	public SituacaoConta getSituacao() {
		return situacao;
	}
	public void setProprietario(Cliente proprietario) {
		this.proprietario = proprietario;
	}
	public Cliente getProprietario() {
		return proprietario;
	}
	public void setTitular(Cliente titular) {
		this.titular = titular;
	}
	public Cliente getTitular() {
		return titular;
	}
}
