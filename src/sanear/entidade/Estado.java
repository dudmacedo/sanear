package sanear.entidade;

public enum Estado {
	AC, AL, AM, AP, BA, CE, DF, ES, GO, MA, MG, MS, MT, PA,
	PB, PE, PI, PR, RJ, RN, RO, RR, RS, SC, SE, SP, TO;
	
	public String getNome() {
		switch(this) {
			case AC: return "Acre";
			case AL: return "Alagoas";
			case AM: return "Amazonas";
			case AP: return "Amap�";
			case BA: return "Bahia";
			case CE: return "Cear�";
			case DF: return "Distrito Federal";
			case ES: return "Esp�rito Santo";
			case GO: return "Goi�s";
			case MA: return "Maranh�o";
			case MG: return "Minas Gerais";
			case MS: return "Mato Grosso do Sul";
			case MT: return "Mato Grosso";
			case PA: return "Par�";
			case PB: return "Para�ba";
			case PE: return "Pernambuco";
			case PI: return "Piau�";
			case PR: return "Paran�";
			case RJ: return "Rio de Janeiro";
			case RN: return "Rio Grande do Norte";
			case RO: return "Rond�nia";
			case RR: return "Roraima";
			case RS: return "Rio Grande do Sul";
			case SC: return "Santa Catarina";
			case SE: return "Sergipe";
			case SP: return "S�o Paulo";
			case TO: return "Tocantins";
		}
		return null;
	}
}
