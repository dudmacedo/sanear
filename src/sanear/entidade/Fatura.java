package sanear.entidade;

import java.io.Serializable;

public class Fatura implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7125065672007120183L;
	private int referencia;
	private double valor;
	private int leit_anterior;
	private int leit_atual;
	private Conta conta;
	private Hidrometro hidrometro;
	private Cliente titular;
	private SituacaoFatura situacao;
	
	public Fatura(int referencia, double valor, int leit_anterior, int leit_atual, Conta conta,
			Hidrometro hidrometro, Cliente titular, SituacaoFatura situacao) {
		this.referencia = referencia;
		this.valor = valor;
		this.leit_anterior = leit_anterior;
		this.leit_atual = leit_atual;
		this.conta = conta;
		this.hidrometro = hidrometro;
		this.titular = titular;
		this.situacao = situacao;
	}

	public int getReferencia() {
		return referencia;
	}
	public double getValor() {
		return valor;
	}
	public int getLeituraAnterior() {
		return leit_anterior;
	}
	public int getLeituraAtual() {
		return leit_atual;
	}
	public int getConsumo() {
		return leit_atual - leit_anterior;
	}
	public Conta getConta() {
		return conta;
	}
	public Hidrometro getHidrometro() {
		return hidrometro;
	}
	public Cliente getTitular() {
		return titular;
	}
	public void setSituacao(SituacaoFatura situacao) {
		this.situacao = situacao;
	}
	public SituacaoFatura getSituacao() {
		return situacao;
	}
}
