package sanear.entidade;

import java.io.Serializable;

public class Hidrometro implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5204834114142711812L;
	private String nu_serie;
	private int medidor;
	private Conta conta;
	
	public Hidrometro(String nu_serie, int medidor) {
		this.nu_serie = nu_serie;
		this.medidor = medidor;
		this.conta = null;
	}
	
	public void setNumeroSerie(String nu_serie) {
		this.nu_serie = nu_serie;
	}
	public String getNumeroSerie() {
		return nu_serie;
	}
	public void setMedidor(int medidor) {
		this.medidor = medidor;
	}
	public int getMedidor() {
		return medidor;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	public Conta getConta() {
		return conta;
	}
}
