package sanear.entidade;

import java.io.Serializable;

public class PessoaFisica extends Cliente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6625544223996659573L;

	public PessoaFisica(String nu_ident, String nome, String endereco, String complemento,
			Cidade cidade) throws SanearEntidadeException {
		super(nu_ident, nome, endereco, complemento, cidade);
	}
	
	@Override
	protected void setNumeroIdentificador(String nu_ident) throws SanearEntidadeException {
		if (validaCPF(nu_ident)) {
			this.nu_ident = nu_ident;
		}
		else {
			throw new SanearEntidadeException("CPF inv�lido.");
		}
	}
	
	public static boolean validaCPF(String cpf) { // 000.000.000-00
		if (cpf.length() == 14) {
			try {
				int soma = 0;
				soma += Integer.parseInt(cpf.substring(0, 1)) * 1;
				soma += Integer.parseInt(cpf.substring(1, 2)) * 2;
				soma += Integer.parseInt(cpf.substring(2, 3)) * 3;
				
				soma += Integer.parseInt(cpf.substring(4, 5)) * 4;
				soma += Integer.parseInt(cpf.substring(5, 6)) * 5;
				soma += Integer.parseInt(cpf.substring(6, 7)) * 6;
				
				soma += Integer.parseInt(cpf.substring(8, 9)) * 7;
				soma += Integer.parseInt(cpf.substring(9, 10)) * 8;
				soma += Integer.parseInt(cpf.substring(10, 11)) * 9;
				
				int dv1 = soma % 11;
				if (dv1 == 10) {
					dv1 = 0;
				}
				
				soma = 0;
				soma += Integer.parseInt(cpf.substring(0, 1)) * 0;
				soma += Integer.parseInt(cpf.substring(1, 2)) * 1;
				soma += Integer.parseInt(cpf.substring(2, 3)) * 2;
				
				soma += Integer.parseInt(cpf.substring(4, 5)) * 3;
				soma += Integer.parseInt(cpf.substring(5, 6)) * 4;
				soma += Integer.parseInt(cpf.substring(6, 7)) * 5;
				
				soma += Integer.parseInt(cpf.substring(8, 9)) * 6;
				soma += Integer.parseInt(cpf.substring(9, 10)) * 7;
				soma += Integer.parseInt(cpf.substring(10, 11)) * 8;
				
				soma += dv1 * 9;
				
				int dv2 = soma % 11;
				if (dv2 == 10) {
					dv2 = 0;
				}
				
				if (Integer.parseInt(cpf.substring(12, 13)) != dv1 || Integer.parseInt(cpf.substring(13, 14)) != dv2) {
					return false;
				}
			} catch (Exception ex) {
				return false;
			}
		}
		return true;
	}
}
