package sanear.entidade;

import java.io.Serializable;

public class PessoaJuridica extends Cliente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7155514795915210799L;

	public PessoaJuridica(String nu_ident, String nome, String endereco, String complemento,
			Cidade cidade) throws SanearEntidadeException {
		super(nu_ident, nome, endereco, complemento, cidade);
	}
	
	@Override
	protected void setNumeroIdentificador(String nu_ident) throws SanearEntidadeException {
		if (validaCNPJ(nu_ident)) {
			this.nu_ident = nu_ident;
		}
		else {
			throw new SanearEntidadeException("CNPJ inv�lido.");
		}
	}
	
	public static boolean validaCNPJ(String cnpj) { // 00.000.000/0000-00
		if (cnpj.length() == 18) {
			try {
				int soma = 0;
				soma += Integer.parseInt(cnpj.substring(0, 1)) * 6;
				soma += Integer.parseInt(cnpj.substring(1, 2)) * 7;
				
				soma += Integer.parseInt(cnpj.substring(3, 4)) * 8;
				soma += Integer.parseInt(cnpj.substring(4, 5)) * 9;
				soma += Integer.parseInt(cnpj.substring(5, 6)) * 2;
				
				soma += Integer.parseInt(cnpj.substring(7, 8)) * 3;
				soma += Integer.parseInt(cnpj.substring(8, 9)) * 4;
				soma += Integer.parseInt(cnpj.substring(9, 10)) * 5;
				
				soma += Integer.parseInt(cnpj.substring(11, 12)) * 6;
				soma += Integer.parseInt(cnpj.substring(12, 13)) * 7;
				soma += Integer.parseInt(cnpj.substring(13, 14)) * 8;
				soma += Integer.parseInt(cnpj.substring(14, 15)) * 9;
				
				int dv1 = soma % 11;
				if (dv1 == 10) {
					dv1 = 0;
				}
				
				soma = 0;
				soma += Integer.parseInt(cnpj.substring(0, 1)) * 5;
				soma += Integer.parseInt(cnpj.substring(1, 2)) * 6;
				
				soma += Integer.parseInt(cnpj.substring(3, 4)) * 7;
				soma += Integer.parseInt(cnpj.substring(4, 5)) * 8;
				soma += Integer.parseInt(cnpj.substring(5, 6)) * 9;
				
				soma += Integer.parseInt(cnpj.substring(7, 8)) * 2;
				soma += Integer.parseInt(cnpj.substring(8, 9)) * 3;
				soma += Integer.parseInt(cnpj.substring(9, 10)) * 4;
				
				soma += Integer.parseInt(cnpj.substring(11, 12)) * 5;
				soma += Integer.parseInt(cnpj.substring(12, 13)) * 6;
				soma += Integer.parseInt(cnpj.substring(13, 14)) * 7;
				soma += Integer.parseInt(cnpj.substring(14, 15)) * 8;
				
				soma += dv1 * 9;
				
				int dv2 = soma % 11;
				if (dv2 == 10) {
					dv2 = 0;
				}
				
				if (Integer.parseInt(cnpj.substring(16, 17)) != dv1 || Integer.parseInt(cnpj.substring(17, 18)) != dv2) {
					return false;
				}
			} catch (Exception ex) {
				return false;
			}
		}
		return true;
	}
}
