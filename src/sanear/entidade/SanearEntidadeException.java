package sanear.entidade;

public class SanearEntidadeException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2205184836470904400L;

	public SanearEntidadeException(String msg) {
		super(msg);
	}
}
