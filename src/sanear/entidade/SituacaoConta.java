package sanear.entidade;

public enum SituacaoConta {
	EM_OPERACAO,
	SUPRIMIDA,
	INATIVA;
}
