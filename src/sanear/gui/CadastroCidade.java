package sanear.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import sanear.entidade.Cidade;
import sanear.entidade.Estado;
import sanear.persistencia.SanearPersistException;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroCidade extends JFrame {
	private Principal pai;
	private JPanel contentPane;
	private JTextField tfNomeCidade;
	private JComboBox cbEstado;
	private JComboBox cbCidade;
	private JButton btnEditar;
	private JComboBox cbEstadoCidade;
	private JButton btnSalvar;
	
	private Cidade edicao = null;
	private JLabel lblStatus;
	private JButton btnCancelar;
	
	public CadastroCidade(Principal pai) {
		setResizable(false);
		setTitle("Cadastro de Cidades");
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
        
        this.pai = pai;
        
        inicializar();
	}
	
	/**
	 * Create the frame.
	 */
	private void inicializar() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 342, 189);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEstado = new JLabel("Estado:");
		lblEstado.setBounds(10, 11, 46, 14);
		contentPane.add(lblEstado);
		
		JLabel lblCidade = new JLabel("Cidade:");
		lblCidade.setBounds(10, 36, 46, 14);
		contentPane.add(lblCidade);
		
		cbCidade = new JComboBox();
		cbCidade.setBounds(54, 36, 262, 20);
		contentPane.add(cbCidade);
		
		cbEstado = new JComboBox();
		cbEstado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setCBCidades();
			}
		});
		cbEstado.setBounds(54, 8, 69, 20);
		
		contentPane.add(cbEstado);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 61, 306, 68);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblEstado_1 = new JLabel("Estado:");
		lblEstado_1.setBounds(10, 36, 46, 14);
		panel.add(lblEstado_1);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(10, 11, 46, 14);
		panel.add(lblNome);
		
		tfNomeCidade = new JTextField();
		tfNomeCidade.setEnabled(false);
		tfNomeCidade.setBounds(54, 8, 242, 20);
		panel.add(tfNomeCidade);
		tfNomeCidade.setColumns(10);
		
		cbEstadoCidade = new JComboBox();
		cbEstadoCidade.setEnabled(false);
		cbEstadoCidade.setBounds(54, 36, 63, 20);
		panel.add(cbEstadoCidade);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salvar();
			}
		});
		btnSalvar.setEnabled(false);
		btnSalvar.setBounds(133, 32, 71, 23);
		panel.add(btnSalvar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cancelar();
			}
		});
		btnCancelar.setEnabled(false);
		btnCancelar.setBounds(214, 32, 82, 23);
		panel.add(btnCancelar);
		
		btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editar((Cidade)cbCidade.getSelectedItem());
			}
		});
		btnEditar.setBounds(133, 7, 89, 23);
		contentPane.add(btnEditar);
		
		JButton btnNova = new JButton("Nova");
		btnNova.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nova();
			}
		});
		btnNova.setBounds(227, 7, 89, 23);
		contentPane.add(btnNova);
		lblStatus = new JLabel("");
		lblStatus.setBounds(10, 140, 306, 14);
		contentPane.add(lblStatus);
		
		loadCombos();
	}
	
	private void cancelar() {
		cbEstado.setEnabled(true);
		cbCidade.setEnabled(true);
		
		tfNomeCidade.setText("");
		tfNomeCidade.setEnabled(false);
		cbEstadoCidade.setEnabled(false);
		btnSalvar.setEnabled(false);
		btnCancelar.setEnabled(false);
		edicao = null;
		
		lblStatus.setText("");
		
		loadCombos();
	}
	private void salvar() {
		if (tfNomeCidade.getText().equals("")) {
			lblStatus.setText("Erro: N�o � permitida a inclus�o com o campo 'Nome' vazio.");
			return;
		}
		if (edicao != null) {
			if (!edicao.getNome().equals(tfNomeCidade.getText()) || edicao.getUF() != (Estado)cbEstadoCidade.getSelectedItem()) { 
				if (pai.persistencia.selectCidade(tfNomeCidade.getText(), (Estado)cbEstadoCidade.getSelectedItem()) == null) {
					edicao.setNome(tfNomeCidade.getText());
					edicao.setUF((Estado)cbEstadoCidade.getSelectedItem());
					lblStatus.setText("Cidade " + edicao.getNome() + " alterada com sucesso.");
				}
				else {
					lblStatus.setText("Erro: J� existe uma cidade com os dados informados.");
					return;
				}
			}
			else {
				lblStatus.setText("N�o houve altera��o.");
			}
		}
		else {
			try {
				pai.persistencia.addCidade(new Cidade(tfNomeCidade.getText(), (Estado)cbEstadoCidade.getSelectedItem()));
			} catch (SanearPersistException ex) {
				lblStatus.setText("Erro: " + ex.getMessage());
				return;
			}
			lblStatus.setText("Cidade: " + tfNomeCidade.getText() + " salva com sucesso.");
		}
		
		cbEstado.setEnabled(true);
		cbCidade.setEnabled(true);
		
		tfNomeCidade.setText("");
		tfNomeCidade.setEnabled(false);
		cbEstadoCidade.setEnabled(false);
		btnSalvar.setEnabled(false);
		btnCancelar.setEnabled(false);
		edicao = null;
		
		loadCombos();
	}
	private void nova() {
		edicao = null;
		
		tfNomeCidade.setText("");
		tfNomeCidade.setEnabled(true);
		cbEstadoCidade.setEnabled(true);
		
		btnSalvar.setEnabled(true);
		btnCancelar.setEnabled(true);
	}
	private void editar(Cidade cidade) {
		edicao = cidade;
		
		cbEstado.setEnabled(false);
		cbCidade.setEnabled(false);
		btnEditar.setEnabled(false);
		
		tfNomeCidade.setEnabled(true);
		tfNomeCidade.setText(cidade.getNome());
		
		cbEstadoCidade.setEnabled(true);
		cbEstadoCidade.setSelectedItem(cidade.getUF());
		
		btnSalvar.setEnabled(true);
		btnCancelar.setEnabled(true);
	}
	
	private void loadCombos() {
		cbEstado.setModel(new DefaultComboBoxModel<>(Estado.values()));
		cbEstado.setSelectedIndex(8);
		
		setCBCidades();
		
		cbEstadoCidade.setModel(new DefaultComboBoxModel<>(Estado.values()));
		cbEstadoCidade.setSelectedIndex(8);
	}
	private void setCBCidades() {
			cbCidade.setModel(new DefaultComboBoxModel<>(new Vector(pai.persistencia.getCidades((Estado)cbEstado.getSelectedItem()))));
		
		if (cbCidade.getSelectedItem() != null) {
			btnEditar.setEnabled(true);
		}
		else {
			btnEditar.setEnabled(false);
		}
	}
}
