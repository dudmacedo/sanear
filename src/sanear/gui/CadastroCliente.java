package sanear.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import sanear.entidade.Cidade;
import sanear.entidade.Cliente;
import sanear.entidade.Estado;
import sanear.entidade.PessoaFisica;
import sanear.entidade.PessoaJuridica;
import sanear.entidade.SanearEntidadeException;
import sanear.persistencia.SanearPersistException;

import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class CadastroCliente extends JFrame {
	private Principal pai;
	private Cliente edicao = null;
	
	private JPanel contentPane;
	private JTextField tfIdentificador;
	private JTextField tfComplemento;
	private JTextField tfEndereco;
	private JTextField tfNome;
	private JComboBox cbEstado;
	private JComboBox cbCidade;
	private JLabel lblStatus;
	private JLabel lblTipoCliente;
	private JButton btnSalvar;
	private JButton btnConsultar;
	private JButton btnIncluir;
	private JButton btnCancelar;
	private JButton btnListar;
	
	public CadastroCliente(Principal pai) {
		setResizable(false);
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
        
        this.pai = pai;
        
        inicializar();
	}

	private void inicializar() {
		setTitle("Cadastro de Clientes");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 653, 251);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCpfcnpj = new JLabel("CPF/CNPJ:");
		lblCpfcnpj.setBounds(10, 11, 59, 14);
		contentPane.add(lblCpfcnpj);
		
		tfIdentificador = new JTextField();
		tfIdentificador.setBounds(79, 8, 141, 20);
		contentPane.add(tfIdentificador);
		tfIdentificador.setColumns(10);
		
		btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultar();
			}
		});
		btnConsultar.setBounds(230, 7, 79, 23);
		contentPane.add(btnConsultar);
		
		btnIncluir = new JButton("Incluir");
		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				incluir();
			}
		});
		btnIncluir.setBounds(319, 7, 66, 23);
		contentPane.add(btnIncluir);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 36, 616, 146);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome * :");
		lblNome.setBounds(10, 39, 46, 14);
		panel.add(lblNome);
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o * :");
		lblEndereo.setBounds(10, 64, 74, 14);
		panel.add(lblEndereo);
		
		JLabel lblComplemento = new JLabel("Complemento:");
		lblComplemento.setBounds(10, 89, 74, 14);
		panel.add(lblComplemento);
		
		JLabel lblEstado = new JLabel("Estado * :");
		lblEstado.setBounds(10, 114, 60, 14);
		panel.add(lblEstado);
		
		JLabel lblCidade = new JLabel("Cidade * :");
		lblCidade.setBounds(171, 114, 60, 14);
		panel.add(lblCidade);
		
		tfComplemento = new JTextField();
		tfComplemento.setEnabled(false);
		tfComplemento.setBounds(94, 86, 512, 20);
		panel.add(tfComplemento);
		tfComplemento.setColumns(10);
		
		tfEndereco = new JTextField();
		tfEndereco.setEnabled(false);
		tfEndereco.setBounds(94, 61, 512, 20);
		panel.add(tfEndereco);
		tfEndereco.setColumns(10);
		
		tfNome = new JTextField();
		tfNome.setEnabled(false);
		tfNome.setBounds(94, 36, 512, 20);
		panel.add(tfNome);
		tfNome.setColumns(10);
		
		cbEstado = new JComboBox();
		cbEstado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setCBCidades();
			}
		});
		cbEstado.setEnabled(false);
		cbEstado.setBounds(94, 111, 67, 20);
		panel.add(cbEstado);
		
		cbCidade = new JComboBox();
		cbCidade.setEnabled(false);
		cbCidade.setBounds(229, 111, 377, 20);
		panel.add(cbCidade);
		
		JLabel lblTpCliente = new JLabel("Tipo Cliente:");
		lblTpCliente.setBounds(10, 11, 74, 14);
		panel.add(lblTpCliente);
		
		lblTipoCliente = new JLabel("");
		lblTipoCliente.setBounds(94, 11, 512, 14);
		panel.add(lblTipoCliente);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salvar();
			}
		});
		btnSalvar.setEnabled(false);
		btnSalvar.setBounds(395, 7, 66, 23);
		contentPane.add(btnSalvar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		btnCancelar.setEnabled(false);
		btnCancelar.setBounds(471, 7, 79, 23);
		contentPane.add(btnCancelar);
		
		btnListar = new JButton("Listar");
		btnListar.setEnabled(false);
		btnListar.setBounds(560, 7, 66, 23);
		contentPane.add(btnListar);
		
		lblStatus = new JLabel("");
		lblStatus.setBounds(10, 193, 616, 14);
		contentPane.add(lblStatus);
		
		loadCombos();
	}
	
	private void salvar() {
		if (tfNome.getText().equals("") ||
				tfEndereco.getText().equals("") ||
				cbCidade.getSelectedItem() == null) {
			lblStatus.setText("Erro: � obrigat�rio o preenchimento de campos com *.");
			return;
		}

		if (edicao == null) {
			Cliente cli = null;
			
			if (tfIdentificador.getText().length() == 14) {
				try {
					cli = new PessoaFisica(
							tfIdentificador.getText(),
							tfNome.getText(),
							tfEndereco.getText(),
							tfComplemento.getText(),
							(Cidade)cbCidade.getSelectedItem());
					
				} catch (SanearEntidadeException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}
			else if (tfIdentificador.getText().length() == 18) {
				try {
					cli = new PessoaJuridica(
							tfIdentificador.getText(),
							tfNome.getText(),
							tfEndereco.getText(),
							tfComplemento.getText(),
							(Cidade)cbCidade.getSelectedItem());
					
				} catch (SanearEntidadeException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
			}
			
			try {
				pai.persistencia.addCliente(cli);
			} catch (SanearPersistException e) {
				lblStatus.setText("Erro: " + e.getMessage());
				return;
			}
			
			cancelar();
			lblStatus.setText("Cliente adicionado com sucesso.");
		}
		else {
			edicao.setNome(tfNome.getText());
			edicao.setEndereco(tfEndereco.getText());
			edicao.setComplemento(tfComplemento.getText());
			edicao.setCidade((Cidade)cbCidade.getSelectedItem());
			
			cancelar();
			lblStatus.setText("Cadastro de cliente atualizado.");
		}
	}
	private void cancelar() {
		edicao = null;
		tfIdentificador.setText("");
		tfIdentificador.setEnabled(true);
		btnConsultar.setEnabled(true);
		btnIncluir.setEnabled(true);
		btnSalvar.setEnabled(false);
		btnCancelar.setEnabled(false);
		btnListar.setEnabled(false);
		
		lblTipoCliente.setText("");
		tfNome.setText("");
		tfNome.setEnabled(false);
		tfEndereco.setText("");
		tfEndereco.setEnabled(false);
		tfComplemento.setText("");
		tfComplemento.setEnabled(false);
		cbEstado.setEnabled(false);
		cbCidade.setEnabled(false);
		
		lblStatus.setText("");
	}
	private void incluir() {
		switch (tfIdentificador.getText().length()) {
		// Pessoa F�sica
		case 14:
			if (!PessoaFisica.validaCPF(tfIdentificador.getText())) {
				lblStatus.setText("O CPF informado � inv�lido.");
				return;
			}
			else {
				lblTipoCliente.setText("Pessoa F�sica");
			}
			break;
		// Pessoa Jur�dica
		case 18:
			if (!PessoaJuridica.validaCNPJ(tfIdentificador.getText())) {
				lblStatus.setText("O CNPJ informado � inv�lido.");
				return;
			}
			else {
				lblTipoCliente.setText("Pessoa Jur�dica");
			}
			break;
		default:
			lblStatus.setText("Valor inv�lido informado para CPF/CNPJ.");
			return;
		}
		
		if (pai.persistencia.selectCliente(tfIdentificador.getText()) != null) {
			lblStatus.setText("J� existe um cliente cadastrado com o CPF/CNPJ informado.");
			return;
		}
		
		tfIdentificador.setEnabled(false);
		tfNome.setText("");
		tfNome.setEnabled(true);
		tfEndereco.setText("");
		tfEndereco.setEnabled(true);
		tfComplemento.setText("");
		tfComplemento.setEnabled(true);
		cbEstado.setEnabled(true);
		cbCidade.setEnabled(true);
		
		btnConsultar.setEnabled(false);
		btnIncluir.setEnabled(false);
		btnSalvar.setEnabled(true);
		btnCancelar.setEnabled(true);
		
		lblStatus.setText("");
		
		edicao = null;
	}
	private void consultar() {
		switch (tfIdentificador.getText().length()) {
			// Pessoa F�sica
			case 14:
				if (!PessoaFisica.validaCPF(tfIdentificador.getText())) {
					lblStatus.setText("O CPF informado � inv�lido.");
					 return;
				}
				break;
			// Pessoa Jur�dica
			case 18:
				if (!PessoaJuridica.validaCNPJ(tfIdentificador.getText())) {
					lblStatus.setText("O CNPJ informado � inv�lido.");
					return;
				}
				break;
			default:
				lblStatus.setText("Valor inv�lido informado para CPF/CNPJ.");
				return;
		}
		
		Cliente cli = pai.persistencia.selectCliente(tfIdentificador.getText());
		if (cli == null) {
			lblStatus.setText("N�o foi encontrado nenhum cliente com o identificador informado.");
		}
		else {
			edicao = cli;
			
			tfIdentificador.setEnabled(false);
			btnConsultar.setEnabled(false);
			btnIncluir.setEnabled(false);
			btnSalvar.setEnabled(true);
			btnCancelar.setEnabled(true);
			
			tfNome.setText(cli.getNome());
			tfNome.setEnabled(true);
			tfEndereco.setText(cli.getEndereco());
			tfEndereco.setEnabled(true);
			tfComplemento.setText(cli.getComplemento());
			tfComplemento.setEnabled(true);
			cbEstado.setSelectedItem(cli.getCidade().getUF());
			cbEstado.setEnabled(true);
			cbCidade.setSelectedItem(cli.getCidade());
			cbCidade.setEnabled(true);
			
			lblStatus.setText("");
		}
	}
	
	private void loadCombos() {
		cbEstado.setModel(new DefaultComboBoxModel<>(Estado.values()));
		cbEstado.setSelectedIndex(8);
		
		setCBCidades();
	}
	private void setCBCidades() {
		cbCidade.setModel(new DefaultComboBoxModel<>(new Vector(pai.persistencia.getCidades((Estado)cbEstado.getSelectedItem()))));
	}
}
