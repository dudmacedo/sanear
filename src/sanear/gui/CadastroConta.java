package sanear.gui;

import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import sanear.entidade.Conta;
import sanear.entidade.Estado;
import sanear.entidade.SituacaoConta;

import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroConta extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7272944292527673680L;
	private Principal pai;
	private Conta edicao = null;
	
	private JPanel contentPane;
	private JTextField tfNumConta;
	private JTextField tfComplemento;
	private JTextField tfEndereco;
	private JTextField tfNumeroProprietario;
	private JTextField tfNumeroTitular;
	private JTextField tfNumHidrometro;
	private JComboBox cbEstado;
	private JComboBox cbCidade;
	private JComboBox cbSituacao;
	private JButton btnCancelar;
	private JButton btnSalvar;
	private JButton btnExcluir;
	private JButton btnIncluir;
	private JButton btnConsultar;
	private JLabel lblStatus;
	private JLabel lblNomeProprietario;
	private JLabel lblNomeTitular;
	private JLabel lblStatusHidrometro;
	
	public CadastroConta(Principal pai) {
		setResizable(false);
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
		
		setTitle("Cadastro de Contas");
		this.pai = pai;
		
		inicializar();
	}
	
	/**
	 * Create the frame.
	 */
	public void inicializar() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 673, 301);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNConta = new JLabel("N\u00BA conta: ");
		lblNConta.setBounds(10, 11, 49, 14);
		contentPane.add(lblNConta);
		
		tfNumConta = new JTextField();
		tfNumConta.setBounds(69, 8, 86, 20);
		contentPane.add(tfNumConta);
		tfNumConta.setColumns(10);
		
		btnConsultar = new JButton("Consultar");
		btnConsultar.setBounds(165, 7, 89, 23);
		contentPane.add(btnConsultar);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.setEnabled(false);
		btnSalvar.setBounds(363, 7, 89, 23);
		contentPane.add(btnSalvar);
		
		btnIncluir = new JButton("Incluir");
		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				incluir();
			}
		});
		btnIncluir.setBounds(264, 7, 89, 23);
		contentPane.add(btnIncluir);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setEnabled(false);
		btnCancelar.setBounds(561, 7, 89, 23);
		contentPane.add(btnCancelar);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 36, 640, 190);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblEndereo = new JLabel("Endere\u00E7o * :");
		lblEndereo.setBounds(10, 11, 74, 14);
		panel.add(lblEndereo);
		
		JLabel lblComplemento = new JLabel("Complemento:");
		lblComplemento.setBounds(10, 36, 74, 14);
		panel.add(lblComplemento);
		
		JLabel lblEstado = new JLabel("Estado * :");
		lblEstado.setBounds(10, 61, 60, 14);
		panel.add(lblEstado);
		
		tfComplemento = new JTextField();
		tfComplemento.setEnabled(false);
		tfComplemento.setBounds(94, 33, 536, 20);
		panel.add(tfComplemento);
		tfComplemento.setColumns(10);
		
		tfEndereco = new JTextField();
		tfEndereco.setEnabled(false);
		tfEndereco.setBounds(94, 8, 536, 20);
		panel.add(tfEndereco);
		tfEndereco.setColumns(10);
		
		cbEstado = new JComboBox();
		cbEstado.setEnabled(false);
		cbEstado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setCBCidades();
			}
		});
		cbEstado.setBounds(94, 58, 74, 20);
		panel.add(cbEstado);
		
		JLabel lblCidade = new JLabel("Cidade * :");
		lblCidade.setBounds(178, 61, 60, 14);
		panel.add(lblCidade);
		
		cbCidade = new JComboBox();
		cbCidade.setEnabled(false);
		cbCidade.setBounds(236, 58, 394, 20);
		panel.add(cbCidade);
		
		JLabel lblCpfcnpjProprietrio = new JLabel("CPF/CNPJ Propriet\u00E1rio * :");
		lblCpfcnpjProprietrio.setBounds(10, 86, 131, 14);
		panel.add(lblCpfcnpjProprietrio);
		
		tfNumeroProprietario = new JTextField();
		tfNumeroProprietario.setEnabled(false);
		tfNumeroProprietario.setBounds(151, 83, 160, 20);
		panel.add(tfNumeroProprietario);
		tfNumeroProprietario.setColumns(10);
		
		JLabel lblCpfcnpjTitular = new JLabel("CPF/CNPJ Titular * :");
		lblCpfcnpjTitular.setBounds(10, 111, 121, 14);
		panel.add(lblCpfcnpjTitular);
		
		tfNumeroTitular = new JTextField();
		tfNumeroTitular.setEnabled(false);
		tfNumeroTitular.setBounds(151, 108, 160, 20);
		panel.add(tfNumeroTitular);
		tfNumeroTitular.setColumns(10);
		
		lblNomeProprietario = new JLabel("");
		lblNomeProprietario.setBounds(321, 86, 309, 14);
		panel.add(lblNomeProprietario);
		
		lblNomeTitular = new JLabel("");
		lblNomeTitular.setBounds(321, 111, 309, 14);
		panel.add(lblNomeTitular);
		
		JLabel lblNDeSrie = new JLabel("N\u00BA de S\u00E9rie do Hidr\u00F4metro:");
		lblNDeSrie.setBounds(10, 136, 136, 14);
		panel.add(lblNDeSrie);
		
		tfNumHidrometro = new JTextField();
		tfNumHidrometro.setEnabled(false);
		tfNumHidrometro.setBounds(151, 133, 160, 20);
		panel.add(tfNumHidrometro);
		tfNumHidrometro.setColumns(10);
		
		lblStatusHidrometro = new JLabel("");
		lblStatusHidrometro.setBounds(321, 136, 309, 14);
		panel.add(lblStatusHidrometro);
		
		JLabel lblSituao = new JLabel("Situa\u00E7\u00E3o * :");
		lblSituao.setBounds(10, 161, 60, 14);
		panel.add(lblSituao);
		
		cbSituacao = new JComboBox();
		cbSituacao.setEnabled(false);
		cbSituacao.setBounds(151, 158, 160, 20);
		panel.add(cbSituacao);
		
		btnExcluir = new JButton("Excluir");
		btnExcluir.setEnabled(false);
		btnExcluir.setBounds(462, 7, 89, 23);
		contentPane.add(btnExcluir);
		
		lblStatus = new JLabel("");
		lblStatus.setBounds(10, 237, 637, 14);
		contentPane.add(lblStatus);
		
		loadCombos();
	}
	
	private void incluir() {
		cancelar();
		edicao = null;
		
		if (!tfNumConta.getText().equals("")) {
			lblStatus.setText("N�o � necess�rio informar o n�mero da conta para inclus�o, o sistema dever� gerar automaticamente.");
		}
	}
	private void cancelar() {
		tfNumConta.setText("");
		tfNumConta.setEnabled(true);
		btnConsultar.setEnabled(true);
		btnIncluir.setEnabled(true);
		btnSalvar.setEnabled(false);
		btnExcluir.setEnabled(false);
		btnCancelar.setEnabled(false);
		
		tfEndereco.setText("");
		tfEndereco.setEnabled(false);
		tfComplemento.setText("");
		tfComplemento.setEnabled(false);
		tfNumeroProprietario.setText("");
		tfNumeroProprietario.setEnabled(false);
		lblNomeProprietario.setText("");
		tfNumeroTitular.setText("");
		tfNumeroTitular.setEnabled(false);
		lblNomeTitular.setText("");
		tfNumHidrometro.setText("");
		tfNumHidrometro.setEnabled(false);
		lblStatusHidrometro.setText("");
		
		lblStatus.setText("");
		
		loadCombos();
	}
	
	private void loadCombos() {
		cbEstado.setModel(new DefaultComboBoxModel<>(Estado.values()));
		cbEstado.setSelectedIndex(8);
		
		cbSituacao.setModel(new DefaultComboBoxModel<>(SituacaoConta.values()));
		cbSituacao.setSelectedItem(SituacaoConta.EM_OPERACAO);
		
		setCBCidades();
	}
	private void setCBCidades() {
		cbCidade.setModel(new DefaultComboBoxModel<>(new Vector(pai.persistencia.getCidades((Estado)cbEstado.getSelectedItem()))));
		
	}
}
