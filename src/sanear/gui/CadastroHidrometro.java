package sanear.gui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import sanear.entidade.Hidrometro;
import sanear.persistencia.SanearPersistException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastroHidrometro extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6686069680664292235L;
	private Hidrometro edicao;
	
	private Principal pai;
	private JPanel contentPane;
	private JTextField tfNumSerie;
	private JTextField tfMedidor;
	private JButton btnConsultar;
	private JButton btnIncluir;
	private JButton btnExcluir;
	private JButton btnSalvar;
	private JButton btnCancelar;
	private JLabel lblStatus;

	public CadastroHidrometro(Principal pai) {
		setResizable(false);
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
		
		setTitle("Cadastro de Hidr\u00F4metros");
		this.pai = pai;
		
		inicializar();
	}

	/**
	 * Create the frame.
	 */
	public void inicializar() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 734, 140);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNDeSrie = new JLabel("N\u00BA de S\u00E9rie:");
		lblNDeSrie.setBounds(10, 11, 67, 14);
		contentPane.add(lblNDeSrie);
		
		tfNumSerie = new JTextField();
		tfNumSerie.setBounds(87, 8, 129, 20);
		contentPane.add(tfNumSerie);
		tfNumSerie.setColumns(10);
		
		btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				consultar();
			}
		});
		btnConsultar.setBounds(226, 7, 89, 23);
		contentPane.add(btnConsultar);
		
		btnIncluir = new JButton("Incluir");
		btnIncluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				incluir();
			}
		});
		btnIncluir.setBounds(325, 7, 89, 23);
		contentPane.add(btnIncluir);
		
		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				salvar();
			}
		});
		btnSalvar.setEnabled(false);
		btnSalvar.setBounds(523, 7, 89, 23);
		contentPane.add(btnSalvar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cancelar();
			}
		});
		btnCancelar.setEnabled(false);
		btnCancelar.setBounds(622, 7, 89, 23);
		contentPane.add(btnCancelar);
		
		btnExcluir = new JButton("Excluir");
		btnExcluir.setEnabled(false);
		btnExcluir.setBounds(424, 7, 89, 23);
		contentPane.add(btnExcluir);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 36, 701, 40);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblMedidor = new JLabel("Medidor:");
		lblMedidor.setBounds(10, 11, 53, 14);
		panel.add(lblMedidor);
		
		tfMedidor = new JTextField();
		tfMedidor.setEnabled(false);
		tfMedidor.setBounds(59, 8, 100, 20);
		panel.add(tfMedidor);
		tfMedidor.setColumns(10);
		
		lblStatus = new JLabel("");
		lblStatus.setBounds(10, 87, 701, 14);
		contentPane.add(lblStatus);
	}
	
	private void salvar() {
		try {
			int medidor = Integer.parseInt(tfMedidor.getText());
			if (medidor < 0) {
				lblStatus.setText("Erro: O valor do medidor deve ser positivo.");
				return;
			}
			
			if (edicao == null) {
				pai.persistencia.addHidrometro(new Hidrometro(tfNumSerie.getText(), medidor));
				cancelar();
				lblStatus.setText("Hidr�metro cadastrado com sucesso.");
			}
			else {
				edicao.setMedidor(medidor);
				cancelar();
				lblStatus.setText("Cadastro de hidr�metro atualizado com sucesso.");
			}
		} catch (NumberFormatException e) {
			lblStatus.setText("Erro: Valor inv�lido para o medidor.");
			return;
		} catch (SanearPersistException e) {
			lblStatus.setText("Erro: " + e.getMessage());
			return;
		}
	}
	private void incluir() {
		if (tfNumSerie.getText().equals("")) {
			lblStatus.setText("O n�mero de s�rie informado � inv�lido.");
			return;
		}
		if (pai.persistencia.selectHidrometro(tfNumSerie.getText()) != null) {
			lblStatus.setText("J� existe um hidr�metro cadastrado com este n�mero de s�rie.");
			return;
		}
		
		tfMedidor.setText("0");
		tfMedidor.setEnabled(true);
		
		tfNumSerie.setEnabled(false);
		btnConsultar.setEnabled(false);
		btnIncluir.setEnabled(false);
		btnSalvar.setEnabled(true);
		btnCancelar.setEnabled(true);
	}
	private void consultar() {
		Hidrometro h = pai.persistencia.selectHidrometro(tfNumSerie.getText());
		if (h == null) {
			lblStatus.setText("N�o foi encontrado nenhum hidr�metro com o n�mero de s�rie informado.");
			return;
		}
		else {
			edicao = h;
			tfMedidor.setText(String.valueOf(h.getMedidor()));
		}
		
		tfNumSerie.setEnabled(false);
		tfMedidor.setEnabled(true);
		
		btnConsultar.setEnabled(false);
		btnIncluir.setEnabled(false);
		btnSalvar.setEnabled(true);
		btnCancelar.setEnabled(true);
		
		lblStatus.setText("");
	}
	private void cancelar() {
		tfNumSerie.setText("");
		tfNumSerie.setEnabled(true);
		btnConsultar.setEnabled(true);
		btnIncluir.setEnabled(true);
		btnExcluir.setEnabled(false);
		btnCancelar.setEnabled(false);
		btnSalvar.setEnabled(false);
		
		tfMedidor.setText("");
		tfMedidor.setEnabled(false);
		
		lblStatus.setText("");
	}
}
