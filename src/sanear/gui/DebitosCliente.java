package sanear.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.ScrollPaneConstants;

public class DebitosCliente extends JFrame {
	private Principal pai;
	private JPanel contentPane;
	private JTextField tfIdentificador;
	private JTable table;

	public DebitosCliente(Principal pai) {
		setResizable(false);
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
		
		setTitle("D\u00E9bitos de Clientes");
		this.pai = pai;
		
		inicializar();
	}
	
	/**
	 * Create the frame.
	 */
	public void inicializar() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 890, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("CPF/CNPJ:");
		label.setBounds(10, 14, 59, 14);
		contentPane.add(label);
		
		tfIdentificador = new JTextField();
		tfIdentificador.setColumns(10);
		tfIdentificador.setBounds(79, 11, 141, 20);
		contentPane.add(tfIdentificador);
		
		JRadioButton rdbtnProprietrio = new JRadioButton("Propriet\u00E1rio");
		rdbtnProprietrio.setBounds(454, 10, 93, 23);
		contentPane.add(rdbtnProprietrio);
		
		JRadioButton rdbtnTitular = new JRadioButton("Titular");
		rdbtnTitular.setBounds(549, 10, 59, 23);
		contentPane.add(rdbtnTitular);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.setBounds(518, 36, 89, 23);
		contentPane.add(btnConsultar);
		
		JCheckBox chckbxDebitosNaoBaixados = new JCheckBox("D\u00E9bitos n\u00E3o baixados");
		chckbxDebitosNaoBaixados.setBounds(10, 36, 141, 23);
		contentPane.add(chckbxDebitosNaoBaixados);
		
		JCheckBox chckbxDebitosBaixados = new JCheckBox("D\u00E9bitos Baixados");
		chckbxDebitosBaixados.setBounds(153, 36, 114, 23);
		contentPane.add(chckbxDebitosBaixados);
		
		JLabel lblNomeCliente = new JLabel("");
		lblNomeCliente.setBounds(230, 14, 218, 14);
		contentPane.add(lblNomeCliente);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 70, 864, 382);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 844, 360);
		panel.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Refer\u00EAncia", "Valor", "Vencimento", "Leit. Anterior", "Leit. Atual", "Consumo", "Conta", "N\u00BA Hidr\u00F4metro", "CPF/CNPJ Titular", "Situa\u00E7\u00E3o"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, Float.class, Object.class, Integer.class, Integer.class, Integer.class, Integer.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setMinWidth(75);
		table.getColumnModel().getColumn(1).setResizable(false);
		table.getColumnModel().getColumn(1).setMinWidth(75);
		table.getColumnModel().getColumn(2).setResizable(false);
		table.getColumnModel().getColumn(2).setMinWidth(75);
		table.getColumnModel().getColumn(3).setResizable(false);
		table.getColumnModel().getColumn(3).setMinWidth(75);
		table.getColumnModel().getColumn(4).setResizable(false);
		table.getColumnModel().getColumn(4).setMinWidth(75);
		table.getColumnModel().getColumn(5).setResizable(false);
		table.getColumnModel().getColumn(5).setMinWidth(75);
		table.getColumnModel().getColumn(6).setResizable(false);
		table.getColumnModel().getColumn(6).setMinWidth(75);
		table.getColumnModel().getColumn(7).setResizable(false);
		table.getColumnModel().getColumn(7).setPreferredWidth(100);
		table.getColumnModel().getColumn(7).setMinWidth(100);
		table.getColumnModel().getColumn(8).setResizable(false);
		table.getColumnModel().getColumn(8).setPreferredWidth(100);
		table.getColumnModel().getColumn(8).setMinWidth(100);
		table.getColumnModel().getColumn(9).setResizable(false);
		table.getColumnModel().getColumn(9).setMinWidth(75);
	}
}
