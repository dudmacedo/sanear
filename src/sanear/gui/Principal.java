package sanear.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;

import sanear.persistencia.Persistencia;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Principal extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5961115241454824990L;
	Persistencia persistencia;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Principal(Persistencia persistencia) {
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				persistencia.close();
			}
		});
		this.persistencia = persistencia;
		
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.updateComponentTreeUI(this);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
        }
		
		setTitle("Sistema Comercial Sanear");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 590, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 916, 21);
		contentPane.add(menuBar);
		
		JMenu mnCadastro = new JMenu("Cadastro");
		menuBar.add(mnCadastro);
		
		JMenuItem mntmCliente = new JMenuItem("Cliente");
		mntmCliente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showCadastroCliente();
			}
		});
		
		JMenuItem mntmCidade = new JMenuItem("Cidade");
		mntmCidade.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showCadastroCidade();
			}
		});
		mnCadastro.add(mntmCidade);
		mnCadastro.add(mntmCliente);
		
		JMenuItem mntmConta = new JMenuItem("Conta");
		mntmConta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showCadastroConta();
			}
		});
		mnCadastro.add(mntmConta);
		
		JMenuItem mntmHidrmetro = new JMenuItem("Hidr\u00F4metro");
		mntmHidrmetro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showCadastroHidrometro();
			}
		});
		mnCadastro.add(mntmHidrmetro);
		
		JMenu mnConsulta = new JMenu("Consulta");
		menuBar.add(mnConsulta);
		
		JMenuItem mntmDbitosNoBaixados = new JMenuItem("D\u00E9bitos de Clientes");
		mntmDbitosNoBaixados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showDebitosCliente();
			}
		});
		mnConsulta.add(mntmDbitosNoBaixados);
		
		JMenuItem menuItem = new JMenuItem("");
		menuItem.setBounds(77, 37, 129, 22);
		contentPane.add(menuItem);
	}
	private void showDebitosCliente() {
		DebitosCliente tela = new DebitosCliente(this);
		tela.setVisible(true);
	}
	private void showCadastroHidrometro() {
		CadastroHidrometro tela = new CadastroHidrometro(this);
		tela.setVisible(true);
	}
	private void showCadastroConta() {
		CadastroConta tela = new CadastroConta(this);
		tela.setVisible(true);
	}
	private void showCadastroCliente() {
		CadastroCliente tela = new CadastroCliente(this);
		tela.setVisible(true);
	}
	private void showCadastroCidade() {
		CadastroCidade tela = new CadastroCidade(this);
		tela.setVisible(true);
	}
}
