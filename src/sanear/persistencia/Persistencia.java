package sanear.persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import sanear.entidade.Cidade;
import sanear.entidade.Cliente;
import sanear.entidade.Estado;
import sanear.entidade.Hidrometro;

public class Persistencia {
	BancoDados bd;
	
	public Persistencia() {
		if (new File("dados.sandb").exists()) {
			try {
				FileInputStream fis = new FileInputStream("dados.sandb");
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				bd = (BancoDados)ois.readObject();
				
				ois.close();
				fis.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
		}
		else {
			bd = new BancoDados();
			bd.cidades = new ArrayList<>();
			bd.clientes = new ArrayList<>();
			bd.contas = new ArrayList<>();
			bd.hidrometros = new ArrayList<>();
		}
	}
	
	public void close() {
		try {
			FileOutputStream fos = new FileOutputStream("dados.sandb");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(bd);
			oos.flush();

			oos.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addHidrometro(Hidrometro hidrometro) throws SanearPersistException {
		if (hidrometro == null) {
			throw new SanearPersistException("Hidr�metro NULL informado.");
		}
		for (Hidrometro h : bd.hidrometros) {
			if (h.getNumeroSerie().equals(hidrometro.getNumeroSerie())) {
				throw new SanearPersistException("O Hidr�metro informado j� foi cadastrado anteriormente.");
			}
		}
		
		bd.hidrometros.add(hidrometro);
	}
	public Hidrometro selectHidrometro(String nu_serie) {
		for (Hidrometro h : bd.hidrometros) {
			if (h.getNumeroSerie().equals(nu_serie)) {
				return h;
			}
		}
		
		return null;
	}
	
	public void addCliente(Cliente cliente) throws SanearPersistException {
		if (cliente == null) {
			throw new SanearPersistException("Cliente NULL informado.");
		}
		for (Cliente c : bd.clientes) {
			if (c.getNumeroIdentificador().equals(cliente.getNumeroIdentificador())) {
				throw new SanearPersistException("O cliente j� foi cadastrado anteriormente.");
			}
		}
		bd.clientes.add(cliente);
	}
	public Cliente selectCliente(String nu_ident) {
		for (Cliente c : bd.clientes) {
			if (c.getNumeroIdentificador().equals(nu_ident)) {
				return c;
			}
		}
		
		return null;
	}
	public Cliente selectCliente(Cliente cliente) {
		for (Cliente c : bd.clientes) {
			if (c.isIgual(cliente)) {
				return c;
			}
		}
		
		return null;
	}
	
	public Cidade selectCidade(String nome, Estado uf) {
		for (Cidade c : bd.cidades) {
			if (c.getNome().equals(nome) && c.getUF() == uf) {
				return c;
			}
		}
		
		return null;
	}
	public void addCidade(Cidade cidade) throws SanearPersistException {
		for (Cidade c : bd.cidades) {
			if (c.getNome().toUpperCase().equals(cidade.getNome().toUpperCase()) && c.getUF() == cidade.getUF()) {
				throw new SanearPersistException("A cidade j� existe");
			}
		}
		bd.cidades.add(cidade);
	}
	public ArrayList<Cidade> getCidades(Estado estado) {
		ArrayList<Cidade> retorno = new ArrayList<>();
		
		for (Cidade c : bd.cidades) {
			if (c.getUF() == estado) {
				retorno.add(c);
			}
		}
		
		return retorno;
	}
	
	
}
