package sanear.persistencia;

public class SanearPersistException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3785115681101067064L;

	public SanearPersistException(String msg) {
		super(msg);
	}
}
